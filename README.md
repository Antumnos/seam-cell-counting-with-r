This is a small project by Michael Berger, linked to his Bachelor's Thesis.

To use this properly, both "seam_cell_counting.R" and "seam_cell_graph.R" have to be
in your directory. But in R/R-Studio, only "seam_cell_graph.R" needs to be open.

Also, you need to use the template.csv file that is provided.
And for the sorting to work properly, have one strain after the other, not timepoint 
after timepoint.

For a tutorial, open SEAM CELL GRAPH TUTORIAL.docx
